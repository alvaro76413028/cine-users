package com.cine.users.cineusers;

import com.cine.users.cineusers.model.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean(name = "test1")
    public Test testBean() {
        return new Test();
    }
}
