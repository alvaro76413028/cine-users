package com.cine.users.cineusers;

import com.cine.users.cineusers.model.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CineUsersApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(CineUsersApplication.class, args);
        Test test = (Test) context.getBean("test1");
        test.imprimir();
    }

}

