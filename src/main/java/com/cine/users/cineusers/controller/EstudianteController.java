package com.cine.users.cineusers.controller;


import com.cine.users.cineusers.api.EstudianteInput;
import com.cine.users.cineusers.model.Test;
import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import com.cine.users.cineusers.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

@RequestMapping(value = "/estudiantes")
@RestController
@RequestScope

@Api(tags = "Rest-Estudiante-Controller", description = "Operaciones sobre estudiante")
public class EstudianteController {

    @Autowired
    private Test test;

    @Autowired
    private EstudianteRepository repository;

    @Autowired
    private EstudianteCreateService estudianteCreateService;

    @Autowired
    private EstudianteMostrarByIdService estudianteMostrarByIdService;

    @Autowired
    private EstudianteMostrarByNombreService estudianteMostrarByNombreService;

    @Autowired
    private EstudianteMostrarByNombreApellidoService estudianteMostrarByNombreApellidoService;

    @Autowired
    private EstudianteUpdateService estudianteUpdateService;


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void probarTest() {
        test.imprimir();
    }

    @ApiOperation(
            value = "Crear un Estudiante"
    )

    @ApiResponses({
            @ApiResponse(code = 201, message = "Estudiante creado"),
            @ApiResponse(code = 404, message = "Estudiante no encontrado")
    })
    @RequestMapping(method = RequestMethod.POST)
    public Estudiante crearEstudiante(@RequestBody EstudianteInput input) {
        estudianteCreateService.setInput(input);
        estudianteCreateService.execute();

        return estudianteCreateService.getEstudiante();
    }

    @ApiOperation(
            value = "Mostrar Estudiante ID"
    )

    @ApiResponses({
            @ApiResponse(code = 201, message = "Estudiante encontrado"),
            @ApiResponse(code = 404, message = "Estudiante no encontrado")
    })
    @RequestMapping(value = "/{idEstudiante}", method = RequestMethod.GET)
    public Estudiante mostrarEstudianteId(@PathVariable("idEstudiante") Integer idEstudiante) {
        return estudianteMostrarByIdService.execute(idEstudiante);
    }

    @ApiOperation(
            value = "Mostrar Estudiante Nombre"
    )

    @ApiResponses({
            @ApiResponse(code = 201, message = "Estudiante encontrado"),
            @ApiResponse(code = 404, message = "Estudiante no encontrado")
    })
    @RequestMapping(method = RequestMethod.GET)
    public List<Estudiante> mostrarEstudianteNombre(@RequestParam("nombreEstudiante") String nombreEstudiante) {
        return estudianteMostrarByNombreService.findEstudiante(nombreEstudiante);
    }

    @ApiOperation(
            value = "Mostrar Estudiante Nombre y Apellido"
    )

    @ApiResponses({
            @ApiResponse(code = 201, message = "Estudiante encontrado"),
            @ApiResponse(code = 404, message = "Estudiante no encontrado")
    })
    @RequestMapping(value = "/datos", method = RequestMethod.GET)
    public List<Estudiante> mostrarEstudianteNombreApellido(@RequestParam("nombreEstudiante") String nE,
                                                            @RequestParam("apellidoEstudiante") String aE) {
        return estudianteMostrarByNombreApellidoService.findEstudiante(nE, aE);
    }

    @ApiOperation(
            value = "Actualizar Estudiante"
    )

    @ApiResponses({
            @ApiResponse(code = 201, message = "Estudiante actualizado"),
            @ApiResponse(code = 404, message = "Estudiante no encontrado")
    })
    @RequestMapping(value = "/{idEstudiante}", method = RequestMethod.PUT)
    public void actualizarEstudiante(@PathVariable("idEstudiante") Integer idE,
                                     @RequestBody EstudianteInput estudianteInput) {
        estudianteUpdateService.updateEstudiante(idE, estudianteInput);
    }

}
