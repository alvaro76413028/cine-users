package com.cine.users.cineusers.controller;

import com.cine.users.cineusers.api.MatriculaInput;
import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.domain.Matricula;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import com.cine.users.cineusers.model.repository.MatriculaRepository;
import com.cine.users.cineusers.service.MatriculaCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Alvaro Ledezma
 */

@RequestMapping(value = "/matriculas")
@RestController
@RequestScope
public class MaticulaController {
    @Autowired
    MatriculaCreateService matriculaCreateService;


    @ApiOperation(value = "Crear Matricula")
    @ApiResponses(
            {
                    @ApiResponse(code = 201, message = "Matricula Creada"),
                    @ApiResponse(code = 404, message = "Not Found")
            }
    )
    @RequestMapping(value = "/{idEstudiante}", method = RequestMethod.POST)
    public Matricula createMatricula(@PathVariable(value = "idEstudiante") Integer id, @RequestBody MatriculaInput input) {

        matriculaCreateService.setId(id);
        matriculaCreateService.setInput(input);
        matriculaCreateService.execute();

        return matriculaCreateService.getMatricula();
    }

    @RequestMapping(value = "/{idEstudiante}", method = RequestMethod.GET)
    public Estudiante buscar(@PathVariable("idEstudiante") Integer id) {
        return matriculaCreateService.buscarEstudiante(id);
    }
}
