package com.cine.users.cineusers.service;

import com.cine.users.cineusers.api.EstudianteInput;
import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alvaro Ledezma
 */

@Service
public class EstudianteCreateService {

    @Autowired
    EstudianteRepository repository;

    Estudiante estudiante;

    EstudianteInput input;

    public void execute() {
        estudiante = crearEstudiante();
    }

    private Estudiante crearEstudiante() {
        Estudiante e = new Estudiante();
        e.setNombreEstudiante(input.getNombreEstudiante());
        e.setApellidosEstudiante(input.getApellidosEstudiante());
        e.setDireccionEstudiante(input.getDireccionEstudiante());

        return repository.save(e);
    }

    public void setInput(EstudianteInput input) {
        this.input = input;
    }

    public Estudiante getEstudiante() {
        return this.estudiante;
    }

}
