package com.cine.users.cineusers.service;

import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alvaro Ledezma
 */

@Service
public class EstudianteMostrarByIdService {

    @Autowired
    EstudianteRepository repository;


    public Estudiante execute(Integer id) {
        return repository.findById(id).orElse(null);
    }

}
