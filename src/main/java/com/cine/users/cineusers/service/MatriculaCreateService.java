package com.cine.users.cineusers.service;

import com.cine.users.cineusers.api.MatriculaInput;
import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.domain.Matricula;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import com.cine.users.cineusers.model.repository.MatriculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alvaro Ledezma
 */
@Service
public class MatriculaCreateService {

    private Matricula matricula;

    private MatriculaInput input;

    private Integer id;


    @Autowired
    private MatriculaRepository matriculaRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;


    public Matricula getMatricula() {
        return this.matricula;
    }

    public void setInput(MatriculaInput input) {
        this.input = input;
    }

    public void execute() {
        this.matricula = crearMatricula();
    }

    private Matricula crearMatricula() {
        Matricula m = new Matricula();
        if (buscarEstudiante(this.id) != null) {
            m.setNivel(input.getNivel());
            m.setAnio(input.getAnio());
            m.setIdEstudiante(buscarEstudiante(this.id));

            matriculaRepository.save(m);
        }
        return m;
    }

    public Estudiante buscarEstudiante(int id) {
        return estudianteRepository.findById(id).orElse(null);
    }

    public void setId(int id) {
        this.id = id;
    }


}
