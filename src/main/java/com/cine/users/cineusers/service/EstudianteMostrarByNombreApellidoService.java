package com.cine.users.cineusers.service;

import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Alvaro Ledezma
 */
@Service
public class EstudianteMostrarByNombreApellidoService {

    @Autowired
    EstudianteRepository repository;

    public List<Estudiante> findEstudiante(String nombre, String apellido) {
        return repository.findByNombreEstudianteAndApellidosEstudiante(nombre, apellido);
    }
}
