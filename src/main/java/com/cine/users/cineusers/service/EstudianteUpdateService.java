package com.cine.users.cineusers.service;

import com.cine.users.cineusers.api.EstudianteInput;
import com.cine.users.cineusers.model.domain.Estudiante;
import com.cine.users.cineusers.model.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Alvaro Ledezma
 */
@Service
public class EstudianteUpdateService {

    @Autowired
    EstudianteRepository repository;


    public void updateEstudiante(Integer id, EstudianteInput input) {
        Estudiante estudiante = repository.findById(id).orElse(null);
        if (estudiante != null) {
            estudiante.setNombreEstudiante(input.getNombreEstudiante());
            estudiante.setApellidosEstudiante(input.getApellidosEstudiante());
            estudiante.setDireccionEstudiante(input.getDireccionEstudiante());

            repository.save(estudiante);
        }
    }

}
