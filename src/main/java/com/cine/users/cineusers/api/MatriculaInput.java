package com.cine.users.cineusers.api;

import com.cine.users.cineusers.model.domain.Estudiante;

/**
 * @author Alvaro Ledezma
 */
public class MatriculaInput {

    private String anio;
    private String nivel;


    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }


}
