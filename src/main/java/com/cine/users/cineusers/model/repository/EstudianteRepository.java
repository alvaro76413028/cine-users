package com.cine.users.cineusers.model.repository;

import com.cine.users.cineusers.model.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Integer> {

    List<Estudiante> findByNombreEstudiante(String nombreEstudiante);

    List<Estudiante> findByNombreEstudianteAndApellidosEstudiante(String nEst, String aEst);
}
