package com.cine.users.cineusers.model.repository;

import com.cine.users.cineusers.model.domain.Matricula;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Alvaro Ledezma
 */
public interface MatriculaRepository extends JpaRepository<Matricula, Integer> {

}
