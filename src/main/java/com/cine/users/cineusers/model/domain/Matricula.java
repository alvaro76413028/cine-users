package com.cine.users.cineusers.model.domain;

import javax.persistence.*;

/**
 * @author Alvaro Ledezma
 */
@Entity
@Table(name = "matricula")
public class Matricula {

    @Id
    @Column(name = "idMatricula", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idMatricula;

    @Column(name = "anio", nullable = false)
    private String anio;

    @Column(name = "nivel", nullable = false)
    private String nivel;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "idEstudiante", referencedColumnName = "idEstudiante", nullable = false)
    private Estudiante idEstudiante;


    public int getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(int idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public Estudiante getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Estudiante idEstudiante) {
        this.idEstudiante = idEstudiante;
    }
}
