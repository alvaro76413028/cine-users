package com.cine.users.cineusers.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "estudiante")
@JsonIgnoreProperties({"hibernateLazyInizializer", "handler"})
public class Estudiante {
    @Id
    @Column(name = "idEstudiante", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int idEstudiante;

    @Column(name = "nombreEstudiante", nullable = false, length = 50)
    private String nombreEstudiante;

    @Column(name = "apellidosEstudiante", nullable = false, length = 50)
    private String apellidosEstudiante;

    @Column(name = "direccionEstudiante", nullable = false, length = 50)
    private String direccionEstudiante;

    public int getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public String getNombreEstudiante() {
        return nombreEstudiante;
    }

    public void setNombreEstudiante(String nombreEstudiante) {
        this.nombreEstudiante = nombreEstudiante;
    }

    public String getApellidosEstudiante() {
        return apellidosEstudiante;
    }

    public void setApellidosEstudiante(String apellidosEstudiante) {
        this.apellidosEstudiante = apellidosEstudiante;
    }

    public String getDireccionEstudiante() {
        return direccionEstudiante;
    }

    public void setDireccionEstudiante(String direccionEstudiante) {
        this.direccionEstudiante = direccionEstudiante;
    }
}
